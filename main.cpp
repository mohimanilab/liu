//
//  main.cpp
//  wminhash
//
//  Created by Liu Cao on 5/17/18.
//  Copyright © 2018 liucao. All rights reserved.
//

#include <iostream>
#include "theoretical_util.hpp"
#include "io.hpp"

void test_prmSrm(){
    std::string str_prmFilename = "/Users/cello/working/Upitt-CMU/Hosein/wminhash/mgf/PRM_plus_selected.mgf";
    prmSrm test(str_prmFilename);
}

int main(int argc, const char * argv[]) {
    //test_pep_mass();
    //test_theoretical_vector();
    //test_prmSrm();
    //test_dotprod_sparse_dense();
    test_score1();
    return 0;
}
