#include "aaCode.hpp"
#include <map>
#include <cmath>
#include <iostream>
#include <string>

void pep2bivector(std::string str_pep, double* &X, double &total_mass){
    //std::fill(aint_prm, aint_prm + k, 0);
    unsigned long k = str_pep.size();
    std::map<char,double> dict_aadict {
        {'G',57.02147},
        {'A',71.03712},
        {'S',87.03203},
        {'P',97.05277},
        {'V',99.06842},
        {'T',101.04768},
        {'C',103.00919+57},
        {'I',113.08407},
        {'L',113.08407},
        {'N',114.04293},
        {'D',115.02695},
        {'Q',128.05858},
        {'K',128.09497},
        {'E',129.04260},
        {'M',131.04049},
        {'H',137.05891},
        {'F',147.06842},
        {'R',156.10112},
        {'Y',163.06333},
        {'W',186.07932}
    };
    for(int i=0;i<k;i++){
        total_mass += dict_aadict[str_pep[i]];
    }
    int n = int(std::ceil(total_mass));
    X = new double[ n + 1 ];
    std::fill(X, X+n+1, 0);
    prm = 0;
    //printf("\n\n");
    for(int i=0;i<k;i++){
    //    printf("%lf,",X[i]);
        prm += dict_aadict[ach_pep[i]];
        X[prm] = 1;
    }
    std::cout << std::endl << std::endl << X[prm] << std::endl;
}

void test_pep2X(){
    std::string str = "TVPSAG";
    
}


